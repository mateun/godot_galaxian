extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var hp = 100
var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Player_hit():
	hp -= 10
	$VBoxContainer/HBoxContainer/lblHPValue.text = str(hp)

func _on_bullet_hit():
	print("go a bullet hit event!")
	score += 33
	$VBoxContainer/HBoxContainer2/lblScoreValue.text = str(score)

func _on_Player_bullet_fired(bullet):
	print("received signal that a bullet has been fired")
	bullet.connect("bullet_hit", self, "_on_bullet_hit")
