extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var speed = 400

signal bullet_hit

# Called when the node enters the scene tree for the first time.
func _ready():
	$shotSound.play()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.y -= speed * delta


func _on_VisibilityNotifier2D_viewport_exited(viewport):
	queue_free()


func _on_Area2D_body_entered(body):
	print("hit enemy? " + body.name)
	if (body.is_in_group("enemies")):
		emit_signal("bullet_hit")
		body.queue_free()
	
	queue_free()


