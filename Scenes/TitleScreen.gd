extends MarginContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var buttonAlpha = 0
var buttonAlphaDirection = 1
var titleAlpha = 0
var titleDir = 1
var beginPulsingPress = false
var beginPulsingTitle = false

# Called when the node enters the scene tree for the first time.
func _ready():
	print("in ready of title screen")
	$VBoxContainer/CenterContainer2/texTitle.modulate = Color(1,1,1, titleAlpha)
	$VBoxContainer/CenterContainer/btnPressSpace.modulate = Color(1,1,1, buttonAlpha)

func pulseSpaceButton(delta):
	buttonAlpha += (0.5 * delta * buttonAlphaDirection)
	if buttonAlpha > 1:
		buttonAlpha = 1
		buttonAlphaDirection = -1
	if buttonAlpha < 0: 
		buttonAlpha = 0
		buttonAlphaDirection = 1
	
	$VBoxContainer/CenterContainer/btnPressSpace.modulate = Color(1, 1, 1, buttonAlpha)

func pulseTitle(delta):
	titleAlpha += (0.03 * delta * titleDir)
	if titleAlpha > 1:
		titleAlpha = 1
		titleDir= -1
	if titleAlpha < 0: 
		titleAlpha = 0
		titleDir = 1
	
	$VBoxContainer/CenterContainer2/texTitle.modulate = Color(1, 1, 1, titleAlpha)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if beginPulsingPress:
		pulseSpaceButton(delta)
	if beginPulsingTitle:
		pulseTitle(delta)
	

func _on_StartTimer_timeout():
	beginPulsingPress = true
	get_tree().change_scene("res://Scenes/Level1.tscn")


func _on_StartTimerTitle_timeout():
	beginPulsingTitle = true
