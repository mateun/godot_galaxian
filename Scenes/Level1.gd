extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (PackedScene) var wave1;
export (PackedScene) var wave2;

var currentWave

var wave = 0;

# Called when the node enters the scene tree for the first time.
func _ready():
	var vpr = get_viewport_rect()
	$Player.position.x = vpr.size.x / 2
	$Player.position.y = vpr.size.y - 64
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
	#var direction = 0
	#if Input.is_action_pressed("ui_left"):
	#    direction = -1
	#if Input.is_action_pressed("ui_right"):
	#    direction = 1

	#rotation += angular_speed * direction * delta

	#var velocity = Vector2.ZERO
	#if Input.is_action_pressed("ui_up"):
	#    velocity = Vector2.UP.rotated(rotation) * speed

	#position += velocity * delta


func _on_Wave1_timeout():
	var w = wave1.instance()
	add_child(w)
	w.position.x = 200
	w.position.y = 5
