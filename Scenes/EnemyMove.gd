extends KinematicBody2D

export var speed = 400

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var velocity = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	#add_to_group("enemies")
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	velocity.y += delta * speed
	var motion = velocity * delta
	move_and_slide(motion)
