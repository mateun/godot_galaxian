extends Area2D

export var playerSpeed = 300
export (PackedScene) var bullet

signal hit;
signal bullet_fired(bullet)

func fire_rocket():
	var b = bullet.instance()
	owner.add_child(b)
	b.position.x = position.x
	b.position.y = position.y - 10
	emit_signal("bullet_fired", b)
	


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_right"):
		position.x += playerSpeed * delta
	if Input.is_action_pressed("ui_left"):
		position.x -= playerSpeed * delta
		
	if Input.is_action_just_pressed("fire"):
		fire_rocket()


func _on_Area2D_body_entered(body):
	if body.is_in_group("enemies"):
		emit_signal("hit")
	
	if body.is_in_group("collectibles"):
		print("got a collectible!!")
	
	body.queue_free()
